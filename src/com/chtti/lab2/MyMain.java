package com.chtti.lab2;

public class MyMain {
    private static final String[] DAY_OF_WEEK = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};
    public static void main(String[] args) {
        for (String day : DAY_OF_WEEK) {
            changeToChinese(day);
        }
    }

    private static void changeToChinese(String day) {
        switch (day) {
            case "Sunday":
                System.out.println("周日");
                break;
            case "Monday":
                System.out.println("周一");
                break;
            case "Tuesday":
                System.out.println("周二");
                break;
            case "Wednesday":
                System.out.println("周三");
                break;
            case "Thursday":
                System.out.println("周四");
                break;
            case "Friday":
                System.out.println("周五");
                break;
            case "Saturday":
                System.out.println("周六");
                break;
        }
    }
}
