package com.chtti.lab85;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class Main85 {
    private static final String ZIP_FILE_NAME = ".\\data\\abc123.zip";
    private static final String FILE_NAME = ".\\data\\test";

    public static void main(String[] args) throws IOException {
        byte[] buffer = new byte[1024];
        try (ZipOutputStream zipOutputStream =
                     new ZipOutputStream(new FileOutputStream(ZIP_FILE_NAME));
             FileInputStream fileInputStream = new FileInputStream(FILE_NAME)) {
            zipOutputStream.putNextEntry(new ZipEntry(FILE_NAME));
            int lengthRead = 0;
            while ((lengthRead = fileInputStream.read(buffer)) > 0) {
                zipOutputStream.write(buffer, 0, lengthRead);
            }
        }
    }
}
