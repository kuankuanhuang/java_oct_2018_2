package com.chtti.lab97;

public class MyThread extends Thread {
    @Override
    public void run() {
        System.out.println("sleeping...");
        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("time's up, wake up");
    }
}
