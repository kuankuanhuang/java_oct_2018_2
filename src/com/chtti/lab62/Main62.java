package com.chtti.lab62;

import com.chtti.lab57.Worker;

public class Main62 {

    public static void main(String[] args) throws Exception{

        new Thread(() -> wokerFunction()).start();
        Thread.sleep(500);
        wokerFunction();

    }

    private static void wokerFunction(){
        for(int i = 0 ; i < 10 ;i++){

            try{
                Thread.sleep(200);
            }catch (Exception ex){
                ex.printStackTrace();
            }

            System.out.println("now processing " + i);
        }

    }
}
