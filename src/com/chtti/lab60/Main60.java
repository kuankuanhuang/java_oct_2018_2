package com.chtti.lab60;

import java.util.Arrays;

public class Main60 {


    private static final String[] TECH = {"deep learning", "machine learning", "tensor flow", "sckit-learn"};
    public static void main(String[] args) {

        System.out.println("Techs :" + Arrays.toString(TECH));
        Arrays.sort(TECH, (o1, o2)->{return o1.length() - o2.length();}
        );
        System.out.println("after sort. Techs :" + Arrays.toString(TECH));

    }
}
