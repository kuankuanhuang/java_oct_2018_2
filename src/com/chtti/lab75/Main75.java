package com.chtti.lab75;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main75 {

    private static final String[] DAY_OF_WEEK = {"Sun", "Mon", "Tues", "Wedn", "Thur", "Fir", "Sat"};

    public static void main(String[] args) throws Exception{

        List<String> dayofweekList = Arrays.asList(DAY_OF_WEEK);
        Stream<String> stream1 = dayofweekList.stream();

        List result1 = stream1.map(String::length)
                .filter(n->n>2)
                .collect(Collectors.toList());

        result1.forEach(System.out::println);
        Stream<String> stream2 = dayofweekList.stream();
        Optional<Integer> firstResult = stream2.map(String::length).filter(n -> n > 7 ).findFirst();

    }


}
