package com.chtti.lab47;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

public class Main47 {


    public static void main(String[] args) {

        String string1 = "abcdefg123456789";
        StringBuffer builder = new StringBuffer("");
        List<Character> list1 = new LinkedList<>();

        for(char stringChar : string1.toCharArray()){
            list1.add(stringChar);
        }

        Iterator<Character> reverse = list1.listIterator(list1.size());
        while(((ListIterator<Character>) reverse).hasPrevious()) {
            char c = ((ListIterator<Character>) reverse).previous();
            builder.append(c);

        }

        System.out.println(builder.toString());
    }
}
