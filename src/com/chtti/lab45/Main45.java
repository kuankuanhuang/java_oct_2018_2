package com.chtti.lab45;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class Main45 {
    public static void main(String[] args) {

        List<String> items = new ArrayList<>();
        items.add("Iphone");
        items.add("black berry");
        for (String item : items ){
            System.out.println("String :" + item );
        }

        for(Iterator<String> iterable = items.listIterator();  iterable.hasNext();){
            String item = iterable.next();
            System.out.println("using iterator, item=" + item );
        }

    }
}
