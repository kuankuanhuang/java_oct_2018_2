package com.chtti.lab55;

import java.util.Arrays;

public class Main55 {
    public static void main(String[] args) {


        Asset mark = new Asset("Mark", 0, 100);
        Asset aaron = new Asset("aaron", 100, 0);
        Asset John = new Asset("John", 50, 50);
        Asset Amy = new Asset("Amy", 75, 25);

        Asset[] assets = {mark, aaron, John, Amy};
        System.out.println("orginal:" + Arrays.toString(assets));
        Arrays.sort(assets);
        System.out.println("sorted:" + Arrays.toString(assets));

    }
}
