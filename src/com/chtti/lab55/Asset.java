package com.chtti.lab55;

public class Asset implements Comparable<Asset>{

    private String name ;
    private int twd ;
    private int usd ;
    private static final int RATE = 31 ;

    public Asset(String name, int twd, int usd) {
        this.name = name;
        this.twd = twd;
        this.usd = usd;
    }

    @Override
    public String toString() {
        return String.format("%s, usd=%d, twd=%d, twd total=%d\n", name, twd, usd, usd * RATE + twd);
    }

    @Override
    public int compareTo(Asset o) {
        int value = twd + usd * RATE;
        int other = o.twd + o.usd * RATE;
        return value - other ;
    }
}
