package com.chtti.lab67;

public class User {
    String name ;

    public User(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return String.format("[%s]", this.name);
    }
}
