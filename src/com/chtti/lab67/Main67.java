package com.chtti.lab67;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main67 {
    public static void main(String[] args) throws Exception{

        List<String> labels = new ArrayList<>();
        labels.add("John");
        labels.add("Ken");
        labels.add("Tim");
        labels.add("Tim");

        Stream<User> stream = labels.stream().map(User::new);
        List<User> users = stream.collect(Collectors.toList());
        for(User user : users ){
            System.out.println(user);

        }
    }
}
