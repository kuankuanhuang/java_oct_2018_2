package com.chtti.lab59;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;



public class Main59 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello JavaFX from chtti");

        Button button = new Button();
        button.setText("Hello world");
        StackPane pane = new StackPane();
        pane.getChildren().add(button);
        primaryStage.setScene(new Scene(pane, 300, 300));
        primaryStage.show();

        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
