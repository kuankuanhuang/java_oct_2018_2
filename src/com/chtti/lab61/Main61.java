package com.chtti.lab61;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;



public class Main61 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello JavaFX from chtti");

        Button button = new Button();
        button.setText("Hello world");

        StackPane pane = new StackPane();
        pane.getChildren().add(button);
        button.setOnAction(
                event -> {
                    System.out.println("Thanks for clicking");
                    System.out.println("Hi this is second command");
                }
        );

        primaryStage.setScene(new Scene(pane, 300, 300));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
