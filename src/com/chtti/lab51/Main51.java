package com.chtti.lab51;

import java.util.HashSet;
import java.util.Set;

public class Main51 {
    public static void main(String[] args) {

        Set<Rectangle> rectangleSets = new HashSet<>();
        rectangleSets.add(new Rectangle(1, 2, 3, 4));
        rectangleSets.add(new Rectangle(5, 6, 7, 8));
        rectangleSets.add(new Rectangle(1, 2, 3, 4));
        System.out.println(rectangleSets);

    }
}
