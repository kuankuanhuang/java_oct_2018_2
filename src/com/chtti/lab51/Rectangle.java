package com.chtti.lab51;

public class Rectangle {
    private int x ;
    private int y ;
    private int width ;
    private int height ;

    public Rectangle(int x, int y, int width, int height) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    @Override
    public String toString() {
        return String.format("origen:[%d, %d], dim[%d, %d]", x, y, width, height);
    }

    @Override
    public boolean equals(Object obj) {
        System.out.println("My own compare function ");
        if(obj == null)
            return false;
        if(obj == this)
            return true;
        if(obj instanceof Rectangle){
            Rectangle rec = (Rectangle)obj;
            if(this.x == rec.x && this.y == rec.y && this.height == rec.height && this.width == rec.width){
                return true ;
            }
        }

        return false;
    }

    @Override
    public int hashCode() {
        System.out.println(x | y <<8  | width << 16 | height << 24);
        return x | y <<8  | width << 16 | height << 24;
    }
}
