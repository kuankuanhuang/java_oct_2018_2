package com.chtti.lab49;

import java.util.Set;
import java.util.TreeSet;

public class Main49 {
    public static void main(String[] args) {
        String erlangString = "Erlang is a programming language used to build massively scalable soft real-time systems with requirements on high availability. Some of its uses are in telecoms, banking, e-commerce, computer telephony and instant messaging. Erlang's runtime system has built-in support for concurrency, distribution and fault tolerance.";
        Set<Character> orderedChar = new TreeSet<>();
        for (char content : erlangString.toCharArray()){
            orderedChar.add(content);
        }
        System.out.println("Used character:"+orderedChar);


    }
}
