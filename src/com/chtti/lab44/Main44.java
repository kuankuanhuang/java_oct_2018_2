package com.chtti.lab44;


import java.util.ArrayList;
import java.util.List;

class Caulator {
    public static Double summation(List<? extends Number> numberList){

        Double result = 0.0 ;

        for(Number number : numberList){
            result+=number.doubleValue();
        }
        return result ;
    }
}

public class Main44 {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        List<Double> list2 = new ArrayList<>();

        for(int i = 0 ; i<5 ; i++){
            list1.add(i);
            list2.add(Math.sqrt((double)i * 100));
        }

        System.out.println("list1=" + list1);
        System.out.println("list2=" + list2);

        System.out.println("list1 sum=" + Caulator.summation(list1));
        System.out.println("list2 sum =" + Caulator.summation(list2));


    }
}
