package com.chtti.lab18;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main18 {
    public static void main(String[] args) {
        String string1 = "Mark:A123456789:0975-111-111, John:D123456789:0977-888-999";
        Pattern pattern = Pattern.compile("[A-Z]\\d{9}");
        Matcher matcher = pattern.matcher(string1);

        while (matcher.find()){
            System.out.println("ID: " + matcher.group());
        }

        Pattern pattern2 = Pattern.compile("\\d{4}-\\d{3}-\\d{3}");
        Matcher matcher2 = pattern2.matcher(string1);
        while (matcher2.find()){
            System.out.println("phone: " + matcher2.group());
        }

    }
}
