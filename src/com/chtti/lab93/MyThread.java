package com.chtti.lab93;

public class MyThread extends Thread {

    @Override
    public void run() {
        try{
            sleep(5000);
            System.out.println("I'm running in thread:" + getName());
        }catch(InterruptedException ex){
            ex.printStackTrace();
        }
    }
}
