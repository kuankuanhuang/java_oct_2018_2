package com.chtti.lab93;

import com.chtti.lab91.WrongIdException;

public class Main93 {
    public static void main(String[] args) throws WrongIdException {
        MyThread thread = new MyThread();
        thread.start();
        System.out.println("In main, I'm running:" + Thread.currentThread().getName());
    }
}
