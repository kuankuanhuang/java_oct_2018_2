package com.chtti.lab78;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main78 {
    public static void main(String[] args) throws Exception {

        Integer[] array1 = {3, 1, 4, 1, 5, 9, 2, 8};
        List<Integer> list1 = Arrays.asList(array1);

        System.out.println("main=" + Collections.min(list1));
        System.out.println("max=" + Collections.max(list1));

        List list2 = new ArrayList(list1);
        list2.add(100);
        list2.add(200);
        System.out.println("after operations=" + list2);

    }

}
