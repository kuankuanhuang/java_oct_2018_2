package com.chtti.lab10;

public class OSDetail {
    private OSType type;

    public OSDetail(OSType osType){
        this.type = osType;
    }

    public void vendor(){
switch (type){
    case IOS:
        System.out.println("Apple ! id=" + type.getId());
        break;
    case ANDROID:
        System.out.println("google ! id=" + type.getId());
        break;
    case WINPHONE:
        System.out.println("already dead ! id=" + type.getId());
        break;
    case BLACKBERRY:
        System.out.println("research in motion ! id=" + type.getId());
        break;
}

    }
}

enum OSType {
    IOS(10), ANDROID(20), WINPHONE(30), BLACKBERRY(40);
    private int id;
    private OSType(int id){
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
