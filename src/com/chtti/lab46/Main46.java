package com.chtti.lab46;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Main46 {

    public static void main(String[] args) {

        List<Integer> items = new ArrayList<>();
        for (int i = 0 ; i < 5 ; i++){
            items.add(i);
        }

        for(Integer item : items){
            System.out.println("item=" + item );
        }

        Iterator<Integer> removeIterator = items.listIterator();
        while(removeIterator.hasNext()){
            Integer result = removeIterator.next();
            if(result % 2 == 0){
                removeIterator.remove();
            }
        }

        for(Iterator<Integer> iterable = items.listIterator();  iterable.hasNext();){
            Integer item = iterable.next();
            System.out.println("using iterator, item=" + item.toString() );

        }

    }
}
