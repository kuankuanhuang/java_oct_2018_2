package com.chtti.lab77;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Main77 {

    public static void main(String[] args) throws Exception{

        Integer[] array1 = {3, 1, 4, 1, 5, 9, 2, 8};
        String[] array2 = {"John", "ken", "Mary", "Tim", "Mark"};

        System.out.println("array1:" + Arrays.toString(array1));
        System.out.println("array2:" + Arrays.toString(array2));

        Arrays.sort(array1);
        Arrays.sort(array2);

        System.out.println("array1:" + Arrays.toString(array1));
        System.out.println("array2:" + Arrays.toString(array2));

        System.out.println("array1:" + Arrays.binarySearch(array1, 1));
        System.out.println("array2:" + Arrays.binarySearch(array2, "TEST"));

    }
}
