package com.chtti.lab8;

public class Employee {

    private static int counter=100;

    public Employee() {
        counter++;
    }

    public static int getCounter() {
        return counter;
    }

}
