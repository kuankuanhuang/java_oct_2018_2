package com.chtti.lab64;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;


public class Main64 extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setTitle("Hello JavaFX from chtti");

        Button button = new Button();
        button.setText("Hello world");

        StackPane pane = new StackPane();
        pane.getChildren().add(button);
        button.setOnAction(System.out::println);

        primaryStage.setScene(new Scene(pane, 300, 300));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
