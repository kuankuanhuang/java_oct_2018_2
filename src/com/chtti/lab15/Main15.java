package com.chtti.lab15;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main15 {
    public static void main(String[] args) {
        String string1 = "Hello world java programming";
        String string2 = "Hello_world_java_programming";
        String string3 = "Hello\\world\\\\java\\\\\\\\programming";
        String string4 = "Hello/world/java/programming";

        //行首符合的意思
        Pattern pattern = Pattern.compile("[^_]+");
        Matcher matcher = pattern.matcher(string2);

        while (matcher.find()){
            System.out.println("parsing result: " + matcher.group());
        }

        Pattern pattern2 = Pattern.compile("[^ ]+");
        Matcher matcher2= pattern2.matcher(string1);

        while (matcher2.find()){
            System.out.println("parsing result: " + matcher2.group());
        }

        Pattern pattern3 = Pattern.compile("[^\\\\]+");
        Matcher matcher3= pattern3.matcher(string3);

        while (matcher3.find()){
            System.out.println(string3 + "\\\\ parsing result: " + matcher3.group());
        }

        Pattern pattern4 = Pattern.compile("[a-zA-Z]+");
        Matcher matcher4= pattern4.matcher(string1);

        while (matcher4.find()){
            System.out.println("parsing result: " + matcher4.group());
        }
        // \\w 任一個字元
        // 中文字元 Pattern pattern5 = Pattern.compile("[\\w]+");
        //Pattern pattern5 = Pattern.compile("[\\w]+", Pattern.UNICODE_CHARACTER_CLASS);
        Pattern pattern5 = Pattern.compile("[\\w]+");
        Matcher matcher5= pattern5.matcher(string1);

        while (matcher5.find()){
            System.out.println("parsing result: " + matcher5.group());
        }

    }
}
