package com.chtti.lab76;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Main76 {

    public static void main(String[] args) throws Exception{

        List<String> versions = new LinkedList<>();

        versions.add("8.0 Oreo");
        versions.add("9.0 pie");
        versions.add("5.1 Lollopop");
        versions.add("6 Mashmallow");
        Collections.sort(versions);

        System.out.println("After sort, versions=" + versions);
        Collections.reverse(versions);
        System.out.println("After reverse, versions=" + versions);

        for(int i = 0 ; i < 4 ; i++){
            Collections.shuffle(versions);
            System.out.println("After shuffle, versions=" + versions);
        }
    }
}
