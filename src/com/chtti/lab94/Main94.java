package com.chtti.lab94;

import com.chtti.lab91.WrongIdException;

public class Main94 {
    public static void main(String[] args) throws WrongIdException {
        Thread thread = new Thread(new MyRunnable());
        thread.start();

        System.out.println("In main, thread name is:" + Thread.currentThread().getName());
    }
}
