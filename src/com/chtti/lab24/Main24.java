package com.chtti.lab24;

import java.io.BufferedWriter;
import java.io.FileWriter;

public class Main24 {
    public static void main(String[] args) throws Exception{
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"));
        for(int i = 0; i < 20 ; i++){
            String lineOutput = String.format("%d, %.4f, %.4f \n", (i+1), Math.pow(i+1,2 ), Math.sqrt((double)i+1));
            bufferedWriter.write(lineOutput);
        }

        bufferedWriter.close();
    }
}
