package com.chtti.lab96;

public class MyRunnable implements Runnable {

    int[] items = {1,2,3,4,5,6,7,8,9,10};

    @Override
    public void run() {

        for(int i = 0 ; i < 10 ; i++){
            try{
                Thread.sleep(500);
            }catch (InterruptedException ex){
                ex.printStackTrace();
            }
            System.out.println("class: " + Thread.currentThread().getName() + ", "+ items[i]);
        }

    }
}
