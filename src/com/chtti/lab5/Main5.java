package com.chtti.lab5;

import com.chtti.lab5.lxus.HybridCar;
import com.chtti.lab5.toyo.Car;
import com.chtti.lab5.toyo.DieselCar;
import com.chtti.lab5.toyo.Driver;

public class Main5 {

    public static void main(String[] args) {

	    // write your code here
        Car car = new Car();
        DieselCar car2 = new DieselCar();
        HybridCar car3 = new HybridCar();

        System.out.println(car);
        System.out.println(car2);
        System.out.println(car2.doSpecial());
        System.out.println("[car3]: from Car toString: " + car3);
        System.out.println(car3.doSpecial());

        Driver driver = new Driver();
        System.out.println(driver.doDrive());

    }
}
