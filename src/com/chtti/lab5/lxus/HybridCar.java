package com.chtti.lab5.lxus;

import com.chtti.lab5.toyo.Car;

public class HybridCar extends Car {

    public HybridCar(){

    }

    public String doSpecial(){
        attr1 = "[DC]:attr1";
        attr3 = "[DC]:attr3";
        String message = String.format("attr1:%s, attr2:%s, attr3:%s, attr4:%s\n",attr1, "----", attr3, "----");
        return message;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
