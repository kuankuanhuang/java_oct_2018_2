package com.chtti.lab5.toyo;

public class Car {

    public String attr1;

    private String attr2;

    protected String attr3;

    String attr4;

    public Car(){
        attr1 = "[Car]attr1";
        attr2 = "[Car]attr2";
        attr3 = "[Car]attr3";
        attr4 = "[Car]attr4";
    }

    @Override
    public String toString() {
        String message = String.format("attr1:%s, attr2:%s, attr3:%s, attr4:%s\n",attr1, attr2, attr3, attr4);
        return message;
    }
}
