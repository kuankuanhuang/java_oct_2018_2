package com.chtti.lab5.toyo;

public class DieselCar extends Car {
    public DieselCar(){

    }

    public String doSpecial(){
        attr1 = "[DC]:attr1";
        attr3 = "[DC]:attr3";
        attr4 = "[DC]:attr4";
        String message = String.format("attr1:%s, attr2:%s, attr3:%s, attr4:%s\n",attr1, "----", attr3, attr4);
        return message;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
