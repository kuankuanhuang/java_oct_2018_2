package com.chtti.lab50;

import java.util.HashMap;
import java.util.Map;

public class Main50 {
    public static void main(String[] args) {

        Map<String, String> phoneBook = new HashMap<>();
        phoneBook.put("aaron", "0000000");
        phoneBook.put("Ker", "111111111111");
        phoneBook.put("aaron", "222222");

        for(String key : phoneBook.keySet()){
            System.out.printf("key:%s, value:%s \n", key, phoneBook.get(key));
        }

    }
}
