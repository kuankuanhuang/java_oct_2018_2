package com.chtti.lab69;

public class Main69 {
    public static void main(String[] args) throws Exception{

        repeatMessage("Hi", 5);
    }

    public static void repeatMessage(String text, int times){

        Runnable r = () -> {

            try{

                while(times > 0 ){

                    System.out.println(times);
                    //times-- ;
                    // lamda 裡面，變數不可以修改值

                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        };
        new Thread(r).start();
    }
}
