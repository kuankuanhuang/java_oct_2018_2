package com.chtti.lab68;

public class Main68 {
    public static void main(String[] args) throws Exception{

        repeatMessage("Hi", 5);
    }

    public static void repeatMessage(String text, int times){

        Runnable r = () -> {

            try{

                for(int i = 0 ; i < times ; i++){

                    Thread.sleep(200);
                    System.out.println(text);

                }

            }catch (Exception ex){
                ex.printStackTrace();
            }
        };
        new Thread(r).start();
    }
}
