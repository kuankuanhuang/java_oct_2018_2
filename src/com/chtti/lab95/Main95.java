package com.chtti.lab95;

import com.chtti.lab91.WrongIdException;
import com.chtti.lab95.MyRunnable;

public class Main95 {

    public static void main(String[] args) throws WrongIdException {
        Thread thread = new Thread(new MyRunnable());
        thread.start();
        try{
            thread.join();
        }catch (Exception ex){
            ex.printStackTrace();
        }

        dosomething();
    }

    static int[] items = {10,9,8,7,6,5,4,3,2,1};

    private static void dosomething(){

        for(int i = 0 ; i < 10 ; i++){
            try{
                Thread.sleep(5000);
            }catch (InterruptedException ex){
                ex.printStackTrace();
            }
            System.out.println(items[i]);
        }
    }
}
