package com.chtti.lab17;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main17 {
    public static void main(String[] args) {
        String string1 = "12315 信義區: 12312 123213 4587897 段號 2 小段 32434234 021234567 09-11213213";
        Pattern pattern = Pattern.compile("\\b\\d{5}\\b");
        Matcher matcher = pattern.matcher(string1);

        while (matcher.find()){
            System.out.println("parsing result: " + matcher.group());
        }
    }
}
