package com.chtti.lab98;

public class MyRunnable implements Runnable {

    private long count ;

    @Override
    public void run() {

        for(int i = 0 ; i < 100 ; i++){
            increment();
            System.out.println("Thread Name:" + Thread.currentThread().getName() + "count: " + count);
        }

    }

    private void increment() {

        // synchronized 不允許多個執行緒同時存取
        synchronized (this){
            long i = count;
            try {
                Thread.sleep(5);
            }catch (InterruptedException ex){
                ex.printStackTrace();
            }
            count = i + 1;
        }
    }
}
