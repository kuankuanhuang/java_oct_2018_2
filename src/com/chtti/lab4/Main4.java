package com.chtti.lab4;

public class Main4 {
    public static void main(String[] args) {
        Employee emp1 = new Employee("Mark");
        System.out.println("phrase1, count="+Employee.getCount());
        Employee emp2 = new Employee("John");
        System.out.println("phrase2, count="+Employee.getCount());
        Employee emp3 = new Employee("Mark");
        System.out.println("phrase3, count="+Employee.getCount());
        Employee emp4 = emp1;
        System.out.println("phrase4-1, count="+Employee.getCount());
        System.out.println("phrase4-2, count="+Employee.getCount());
        System.out.println("phrase4-3, count="+Employee.getCount());
        System.out.println("phrase4-4, count="+Employee.getCount());
        System.out.println(emp1.getName());
        System.out.println(emp2.getName());
        System.out.println(emp1.toString());
        System.out.println(emp2.toString());
        System.out.println(emp3.toString());
        System.out.println(emp4.toString());
        System.out.println(emp1 == emp4);
        System.out.println(emp1 == emp3);
        System.out.println(String.format("the format is ==> %s", emp1));
        emp1.status();
                emp2.status();
        emp3.status();
                emp4.status();

    }
}
