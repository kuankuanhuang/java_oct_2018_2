package com.chtti.lab4;

public class Employee {
    private int idNumner ;
    private String name;
    private int department;
    private static int counter;

    public Employee(String name, int idNumner,  int department) {
        counter++;
        this.name = name;
        this.idNumner = idNumner;
        this.department = department;

    }

    public Employee(String name) {
        this(name, -999, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void status() {
        System.out.println("[normal]I'm fine. ");
        selfTest();
    }

    private void selfTest() {
        for(int i=0;i<10;i++){
            System.out.println("check if I work correctly. ");
        }
    }

    public int getIdNumner() {
        return idNumner;
    }

    public int getDepartment() {
        return department;
    }

    public void setIdNumner(int idNumner) {
        this.idNumner = idNumner;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public static int getCount() {
        return counter;
    }
}
