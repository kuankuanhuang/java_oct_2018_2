package com.chtti.lab22;

import java.io.FileReader;

public class Main22 {
    public static void main(String[] args) throws Exception{
        FileReader reader = new FileReader("README.md");
        int ch ;
        while ((ch =reader.read() )!= -1 ){
            System.out.print((char) ch);
        }

        reader.close();
    }
}
