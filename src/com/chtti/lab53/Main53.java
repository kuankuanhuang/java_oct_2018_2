package com.chtti.lab53;

import java.util.LinkedList;
import java.util.Queue;

public class Main53 {

    public static void main(String[] args) {

        Queue<String> queue = new LinkedList<>();
        queue.add("Apple");
        queue.add("Boo");
        queue.add("Cat");
        queue.add("Duck");

        System.out.println("original : " + queue);

        while(!queue.isEmpty()){
            System.out.println("now : " + queue.remove());
        }
    }
}
