package com.chtti.lab7;

public class Employee {

    private String name ;
    final int gradeLevel = 100;

    public Employee() {
        this("default user");
    }

    public Employee(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getGradeLevel() {
        return gradeLevel;
    }
}
