package com.chtti.lab7;

public class Main7 {
    public static void main(String[] args) {
        int x = 8;
        final int y ;
        System.out.println("x = " + x);
        y=5;
        System.out.println("y = " + y);

        Employee emp1 = new Employee("Mark") ;
        //emp1.gradeLevel = 50;
        System.out.println("emp1 gradeLevel " + emp1.gradeLevel );

        Employee emp2 = new Employee("John") ;
        //emp2.gradeLevel = 70;
        System.out.println("emp2 gradeLevel " + emp2.gradeLevel );

        final Employee emp3 = new Employee("Aaron") ;
        //emp3.gradeLevel = 80;
        System.out.println("emp3 gradeLevel " + emp3.gradeLevel );


    }
}
