package com.chtti.lab58;

import java.util.Arrays;

public class Main58 {


    private static final String[] TECH = {"deep learning", "machine learning", "tensor flow", "sckit-learn"};
    public static void main(String[] args) {

        System.out.println("Techs :" + Arrays.toString(TECH));
        Arrays.sort(TECH, new StringLengthComparator());
        System.out.println("after sort. Techs :" + Arrays.toString(TECH));

    }
}
