package com.chtti.lab52;

import java.util.NavigableMap;
import java.util.TreeMap;

public class Main52 {
    public static void main(String[] args) {

        NavigableMap<Integer, String> ranking = new TreeMap<>();
        ranking.put(90, "Tokyo");
        ranking.put(75, "Taipei");
        ranking.put(60, "Kaohsiung");
        ranking.put(50, "Taichung");
        ranking.put(40, "Osaka");

        System.out.println("desc order:" + ranking.descendingMap());
        System.out.println("over 60:" + ranking.tailMap(59));
        System.out.println("over 60:" + ranking.tailMap(60, true));
        System.out.println("over 60:" + ranking.tailMap(60, false));
        System.out.println("first entry:" + ranking.firstEntry());

    }
}
