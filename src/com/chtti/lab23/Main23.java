package com.chtti.lab23;

import java.io.BufferedReader;
import java.io.FileReader;

public class Main23 {
    public static void main(String[] args) throws Exception{
        FileReader reader = new FileReader("README.md");
        BufferedReader bufferedReader = new BufferedReader(reader);

        String result ;
        while ((result = bufferedReader.readLine()) != null){
            System.out.print(result);
        }

        reader.close();
    }
}
