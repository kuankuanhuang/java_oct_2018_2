package com.chtti.lab12;

public class Main12 {
    public static void main(String[] args) {
        String prolog = "Prolog is a general purpose logic programming language ";
        String matching1 = "FIND:Pro";
        String matching2 = "FIND:is";
        String matching3 = "FIND:IS";

        System.out.println("finding " + prolog.regionMatches(0, matching1, 5, 3));
        System.out.println("finding " + prolog.regionMatches(7, matching2, 5, 2));
        System.out.println("finding " + prolog.regionMatches(true, 7, matching3, 5, 2));
    }
}
