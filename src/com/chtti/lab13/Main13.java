package com.chtti.lab13;

public class Main13 {
    public static void main(String[] args) {
        String booleanString = String.valueOf(true);
        String booleanString2 = String.valueOf(false);
        System.out.println("booleanString1:" + booleanString + ", booleanString2:" + booleanString2);
        System.out.println(String.valueOf(357) + "," + String.valueOf(3.2f) + ", " + String.valueOf(2.317));

        boolean b1 = Boolean.parseBoolean(booleanString);
        boolean b2 = Boolean.parseBoolean(booleanString2);

        int int3 = Integer.parseInt(String.valueOf(357));
        float float4 = Float.parseFloat(String.valueOf(3.2f));
        double double5 = Double.parseDouble(String.valueOf(2.317));

        System.out.println(b1 + "," + b2);
        System.out.println(int3);
        System.out.println(float4);
        System.out.println(double5);

    }
}
