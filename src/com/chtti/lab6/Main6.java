package com.chtti.lab6;

import com.chtti.lab6.Employee;

public class Main6 {
    public static void main(String[] args) {

        //Employee emp1 = new Engineer("Mark");
        //Employee emp2 = new HumanResource("Emily");
        //Employee emp3 = new PM("James");
        Employee[] employees = {new Engineer("Mark"), new HumanResource("Emily"), new PM("James")};
        for (Employee emp : employees) {
            System.out.println(emp.getName() + " created");
            emp.working();
        }
    }
}
