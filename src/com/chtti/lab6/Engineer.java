package com.chtti.lab6;

public class Engineer extends Employee {

    public Engineer(String name){
        super(name);
    }

    @Override
    public void working() {
        System.out.println("R&D, not repeating and debuging ");
    }
}
