package com.chtti.lab6;

public abstract class Employee {
    private int idNumner ;
    private String name;
    private int department;
    private static int counter;
    public abstract void working();

    public Employee(String name, int idNumner, int department) {
        counter++;
        this.name = name;
        this.idNumner = idNumner;
        this.department = department;
    }

    public Employee(String name) {
        this(name, -999, 0);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getIdNumner() {
        return idNumner;
    }

    public int getDepartment() {
        return department;
    }

    public void setIdNumner(int idNumner) {
        this.idNumner = idNumner;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

}
