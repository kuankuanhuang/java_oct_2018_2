package com.chtti.lab19;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main19 {
    public static void main(String[] args) {
        String string1 = "+886-999-888-111, +886-922-333-444";
        Pattern pattern = Pattern.compile("\\+(\\d{3})-(\\d{3})-(\\d{3})-(\\d{3})");
        Matcher matcher = pattern.matcher(string1);
        String stringNew = matcher.replaceAll("0$2-$3-$4");
        System.out.println("Result: " + stringNew);
    }
}
