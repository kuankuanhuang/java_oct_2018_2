package com.chtti.lab21;

import java.io.Console;

public class Main21 {
    public static void main(String[] args) {
        Console console = System.console();
        if(console == null){
            System.out.println("Can not get console !!");
            System.exit(-1);
        }
        console.printf("!!!" + console.readPassword()+"!!!");
    }
}
