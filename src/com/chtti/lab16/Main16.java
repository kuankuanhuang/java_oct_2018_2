package com.chtti.lab16;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main16 {
    public static void main(String[] args) {
        String string1 = "信義區: 123123 123213 4587897 段號 2 小段 32434234";
        Pattern pattern = Pattern.compile("\\d{5}");
        Matcher matcher = pattern.matcher(string1);

        while (matcher.find()){
            System.out.println("parsing result: " + matcher.group());
        }
    }
}
