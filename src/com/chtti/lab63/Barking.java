package com.chtti.lab63;

@FunctionalInterface
public interface Barking {
    void barking(int times);
}
