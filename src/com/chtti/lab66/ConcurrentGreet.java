package com.chtti.lab66;

public class ConcurrentGreet extends Greet {
    @Override
    public void greet() {
        System.out.println("[son](Greet):Hello world !!");
    }

    public void send1(){
        Thread t = new Thread(super::greet);
        t.start();
    }

    public void send2(){
        Thread t = new Thread(this::greet);
        t.start();
    }

}
