package com.chtti.lab66;

import com.chtti.lab65.Caculating;

public class Main66 {
    public static void main(String[] args) throws Exception{

        Greet g1 = new Greet();
        g1.greet();

        Greet g2 = new ConcurrentGreet();
        g2.greet();

        ConcurrentGreet g3 = new ConcurrentGreet();
        g3.send1();
        g3.send2();

    }
}
