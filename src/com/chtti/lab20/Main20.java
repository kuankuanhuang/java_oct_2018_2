package com.chtti.lab20;

public class Main20 {
    public static void main(String[] args) {
        Double pi = 3.141592687;
        System.out.printf("%e,%f\n", pi, pi);
        Double ln2 = 6.931471862e-01;
        System.out.printf("%.5e, %.3f\n", ln2, ln2);
        for (int i = 1; i < 20; i++) {
            System.out.printf("%5d,%10.1f, %20.4f\n", i, Math.pow(i, 2), Math.pow(i, 0.5));
        }
        for (int i = 1; i < 20; i++) {
            System.out.printf("%-5d,%-10.1f,%-20.4f\n", i, Math.pow(i, 2), Math.pow(i, 0.5));
        }
    }
}
