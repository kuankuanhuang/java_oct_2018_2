package com.chtti.lab56;

import java.util.Comparator;


class TotalComparatr implements Comparator<Asset>{

    @Override
    public int compare(Asset o1, Asset o2) {
        return o1.getTotal() - o2.getTotal();
    }
}

public class Asset  {

    private String name ;
    private int twd ;
    private int usd ;
    private static final int RATE = 31 ;

    public Asset(String name, int twd, int usd) {
        this.name = name;
        this.twd = twd;
        this.usd = usd;
    }

    @Override
    public String toString() {
        return String.format("%s, usd=%d, twd=%d, twd total=%d\n", name, twd, usd, usd * RATE + twd);
    }

    public int getTotal() {
        return usd * RATE + twd;
    }


}
