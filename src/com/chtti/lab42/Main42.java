package com.chtti.lab42;

import java.util.ArrayList;
import java.util.List;

class Autofill {
    public static <T> void fill(List<T> list, T value) {
        for (int i = 0; i < list.size(); i++) {
            list.set(i, value);
        }
    }
}


public class Main42 {
    public static void main(String[] args) {
        List<Integer> list1 = new ArrayList<>();
        list1.add(300);
        list1.add(200);
        list1.add(400);
        System.out.println("original list:" + list1);
        Autofill.fill(list1, 777);
        System.out.println("original list:" + list1);
        List<String> list2 = new ArrayList<>();
        list2.add("hihi");
        list2.add("hello world");
        list2.add("anything?");
        System.out.println("original list2:" + list2);
        Autofill.fill(list2, "XXYYZZZ");
        System.out.println("original list2:" + list2);



    }
}
