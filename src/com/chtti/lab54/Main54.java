package com.chtti.lab54;

import java.util.LinkedList;
import java.util.Queue;

public class Main54 {
    public static void main(String[] args) {

        Queue<String> queue = new LinkedList<>();
        queue.add("Apple-100");
        queue.add("Boo-70");
        queue.add("Cat-60");
        queue.add("Duck-50");

        if(((LinkedList<String>) queue).pollFirst() != null
            && ((LinkedList<String>) queue).pollLast() != null){
            System.out.println("after poll firs and last :" + queue);
        }
    }
}
