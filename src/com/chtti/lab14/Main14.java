package com.chtti.lab14;

public class Main14 {
    public static void main(String[] args) {
        String string1 = "Hello world java programming";
        String string2 = "Hello_world_java_programming";
        String string3 = "Hello\\world\\java\\programming";
        String string4 = "Hello/world/java/programming";
        for (String string : string1.split(" ")) {
            System.out.printf("[%s] ", string);
        }
        System.out.println();
        for (String string : string2.split("_")) {
            System.out.printf("[%s] ", string);
        }
        System.out.println();
        for (String string : string3.split("\\\\")) {
            System.out.printf("[%s] ", string);
        }
        System.out.println();
        for (String string : string4.split("/")) {
            System.out.printf("[%s] ", string);

        }
        System.out.println();
    }
}
